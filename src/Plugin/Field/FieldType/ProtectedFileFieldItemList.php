<?php

namespace Drupal\protected_file\Plugin\Field\FieldType;

use Drupal\file\Plugin\Field\FieldType\FileFieldItemList;

/**
 * Defines an item list class for protected_file fields.
 */
class ProtectedFileFieldItemList extends FileFieldItemList {

}
