<?php

namespace Drupal\protected_file\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\file\FileInterface;

/**
 * Class ProtectedFileAccessEvent.
 *
 * @package Drupal\protected_file\Event
 */
class ProtectedFileAccessEvent extends Event {

  /**
   * The access result.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $accessResult;

  /**
   * The uri that is being access checked.
   *
   * @var string
   */
  protected $uri;

  /**
   * The file that is being access checked.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $file;

  /**
   * The host entity of the file that is being access checked.
   *
   * @var \Drupal\Core\Entity\FieldableEntityInterface|NULL
   */
  protected $entity;

  /**
   * ProtectedFileAccessEvent constructor.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $accessResult
   *   The access result.
   * @param string $uri
   *   The uri that is being access checked.
   * @param \Drupal\file\FileInterface $file
   *   The file that is being access checked.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|NULL $entity
   *   The host entity of the file that is being access checked.
   */
  public function __construct(AccessResultInterface $accessResult, string $uri, FileInterface $file, FieldableEntityInterface $entity = NULL) {
    $this->accessResult = $accessResult;
    $this->uri = $uri;
    $this->file = $file;
    $this->entity = $entity;
  }

  /**
   * Returns the access result.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function getAccessResult() {
    return $this->accessResult;
  }

  /**
   * Sets the access result.
   *
   * @param \Drupal\Core\Access\AccessResultInterface $accessResult
   *   The access result.
   */
  public function setAccessResult(AccessResultInterface $accessResult) {
    $this->accessResult = $accessResult;
  }

  /**
   * Returns the uri.
   *
   * @return string
   *   The uri.
   */
  public function getUri() {
    return $this->uri;
  }

  /**
   * Sets the uri.
   *
   * @param string $uri
   *   The uri.
   */
  public function setUri($uri) {
    $this->uri = $uri;
  }

  /**
   * Returns the file.
   *
   * @return \Drupal\file\FileInterface
   *   The file.
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * Sets the file.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file.
   */
  public function setFile(FileInterface $file) {
    $this->file = $file;
  }

  /**
   * Returns the host entity.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface|NULL
   *   The host entity or NULL if not provided.
   */
  public function getEntity() {
    return $this->entity;
  }

}
