<?php

namespace Drupal\protected_file\Event;

/**
 * Provides event names for protected file related events.
 *
 * @package Drupal\protected_file\Event
 */
final class ProtectedFileEvents {

  /**
   * Name of the event fired after checking file access in hook_file_download().
   *
   * @see file_file_download().
   *
   * This event allows modules to provide
   * custom access checks for protected files.
   *
   * @Event
   *
   * @var string
   */
  const CHECK_ACCESS = 'protected_file.check_access';

}
