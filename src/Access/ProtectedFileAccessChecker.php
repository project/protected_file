<?php

namespace Drupal\protected_file\Access;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\file\FileInterface;

/**
 * Checks if access to a file should be granted.
 *
 * @package Drupal\protected_file\Access
 */
class ProtectedFileAccessChecker {

  /**
   * Translates the result of hook_file_download() to an access result.
   *
   * @param array|int $headers
   *   The result of a hook_file_download() function.
   *   Access denied: -1
   *   Access allowed: Array of headers.
   *
   * @see hook_file_download()
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function getAccessResultFromHeaders($headers) {
    if ($headers === -1) {
      return new AccessResultForbidden();
    }
    else {
      return new AccessResultAllowed();
    }
  }

  /**
   * Translates an access result to the result of hook_file_download() .
   *
   * @param \Drupal\Core\Access\AccessResultInterface $accessResult
   *   An access result.
   * @param \Drupal\file\FileInterface $file
   *   A file to generate the appropriate content headers
   *   for download.
   *   In case of an allowed access result.
   *
   * @see hook_file_download()
   *
   * @return array|int
   *   The result of a hook_file_download() function.
   *   Access denied: -1
   *   Access allowed: Array of headers.
   */
  public function getHeadersFromAccessResult(AccessResultInterface $accessResult, FileInterface $file) {
    if ($accessResult->isAllowed()) {
      return file_get_content_headers($file);
    }
    else {
      return -1;
    }
  }

}
